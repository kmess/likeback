
  <div class="subBar {$subBarType}">
{if ! $isHome}
    <a href="view.php">
      <img src="icons/gohome.png" width="32" height="32" alt="Homepage image">
    </a>
{/if}
    &nbsp; &nbsp;
    <strong>{$subBarContents}</strong>
  </div>

