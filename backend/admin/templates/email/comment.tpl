Hello {$project} developer(s),

A user sent a new comment via LikeBack:

> Version: {$fullVersion}
> Locale:  {$locale}
{"Window:  $window"|wrapQuote}{* window may be quite long and therefore wrap over lines*}

> Type:    {$type}
> Comment:
{$comment|wrapQuote}

To reply to this comment or to change its status, please use this link:
{$url}

Thank you,
{$project}'s developing servant, LikeBack
