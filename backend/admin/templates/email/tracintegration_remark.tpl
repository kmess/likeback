Hi!

The automatic LikeBack Trac integration service closed your LikeBack feedback message. This is the message you sent us:

{$comment->comment|wrapQuote}

This comment was linked to Trac bug #{$ticketid}, found at <{$tracurl}>. The integration service closed your comment and set the resolution to {$newResolution|message:'resolution'}, saying:

{$remark|wrapQuote}

To reply to this message, you can simply use the Reply function of your e-mail client. Alternatively, you can use the original LikeBack form within the application.

Thank you for using LikeBack!
The {$project} developers
